<?php
require 'vendor/autoload.php';

use Dompdf\Dompdf;
use Dompdf\Options;

$html =
'<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="fonts/Lato/latofonts.css" />
    <link rel="stylesheet" href="styles.css" />
</head>
<body>
<div class="test">Test</div>
<h1>Dompdf is an HTML to PDF converter</h1>
    
<p>At its heart, dompdf is (mostly) a CSS 2.1 compliant HTML layout and rendering engine written in PHP. It is a style-driven renderer: it will download and read external stylesheets, inline style tags, and the style attributes of individual HTML elements. It also supports most presentational HTML attributes.</p>

<h1>The quick brown fox jumps over the lazy dog</h1>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

<h1>Typography</h1>

<p>Letterforms have tone, timbre, character, just as words and sentences do. The moment a text and a typeface are chosen, two streams of thought, two rhythmical systems, two sets of habits, or if you like, two personalities intersect. They must not live together contentedly forever, but they must not as rule collide.</p>

<p>ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz&0123456789$€!?@#%§.;:,{}</p>

<footer>
    &copy; 2017 Typomedia Software. Alle Rechte vorbehalten. Handmade with &hearts; in Heidelberg.
</footer>
</body>
</html>';

$options = new Options();
$options->set('defaultFont', 'Courier');
$dompdf = new Dompdf($options);
$dompdf->loadHtml($html);
$dompdf->setPaper("a4", 'portrait');
$dompdf->render();
$dompdf->stream("demo.pdf", array("Attachment" => 0));
